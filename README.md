rokka-uploader
==============

Monitors folders for new or changed images, uploads them to [rokka][].


Installation
------------

To install rokka-uploader, you need to have [Node.js][] installed.
rokka-uploader requires Node.js version 10.14 or higher.

We recommend you install the latest LTS release, since that is what
rokka-uploader is tested with.

With Node.js installed, run this command in your favourite shell:

    npm install -g @pistor/rokka-uploader 


Configuration/usage
-------------------

rokka-uploader uses [dotenv][] for configuration. That gives you two
options for configuration, namely a `.env` file, or environment variables.

Each is described in more detail below.

Regardless of how you configure rokka-uploader, you will need to configure
at least two values, `ROKKA_API_KEY` and `ROKKA_ORGANISATION`. All other
settings have reasonable defaults.


### Using a `.env` file for configuration

This method involves placing a .env file in the folder where you run
`rokka-uploader`. That could be the folder containing the images you want
uploaded, this folder, or any other folder.

As example, we will pick the folder `/opt/rokka-uploader`. Make a copy of
the `.env.example` file in this repository in the picked folder, rename it
to `.env` and adjust the configuration as necessary.

When that is done, start `rokka-uploader` like this:

    cd /opt/rokka-uploader
    rokka-uploader
    
If you did not set `MONITOR_GLOB` in your `.env` file, rokka-uploader will
now upload any .gif, .jpg, .jpeg, or .png file it finds in the folder where
you started it, in our example `/opt/rokka-uploader`.


### Using environmental variables

There’s a number of ways to set environmental variables, but explaining them
all is beyond the scope of this article, so we will just show the simplest
example, namely specifying them directly on the command line, by running
rokka-uploader like this:

    ROKKA_API_KEY=your-key-here ROKKA_ORGANISATION=your-name-here MONITOR_GLOB=/your/images/*.jpg rokka-uploader
    
In this example, we specify `MONITOR_GLOB` so rokka-uploader will look in the
folder `/your/images` for .jpg files and upload them.

Like with the `.env` file method, if you do not specify `MONITOR_GLOB`,
rokka-uploader will upload any .gif, .jpg, .jpeg, or .png file it finds in
the folder where you started it.

Refer to `.env.example` to see what options are available.

[dotenv]: https://github.com/motdotla/dotenv
[rokka]: https://rokka.io/
[Node.js]: https://nodejs.org/
