module.exports = {
  apps: [
    {
      name: 'rokka-mercanto',
      script: 'rokka-uploader',
      cwd: '/APP/mercanto-rokka-uploader',
      log_file: '/APP/mercanto-rokka-uploader/mercanto-rokka-uploader.log',
      instances: 1,
      autorestart: true,
      watch: false
    },
    {
      name: 'rokka-pistor-ch',
      script: 'rokka-uploader',
      cwd: '/APP/pistorch-rokka-uploader',
      log_file: '/APP/pistorch-rokka-uploader/pistorch-rokka-uploader.log',
      instances: 1,
      autorestart: true,
      watch: false
    }
  ]
}
