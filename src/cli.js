const Bottleneck = require('bottleneck')
const dotenv = require('dotenv')
const dotenvParseVariables = require('dotenv-parse-variables')
const path = require('path')
const SftpClient = require('ssh2-sftp-client')
const uniqid = require('uniqid')
const { tmpdir } = require('os')
const { promises } = require('fs')
const { getLogger } = require('./logger')
const { handleFileAdd, handleFileChange } = require('./fileChangeProcessing')
const { validate } = require('./config')
const { watch } = require('chokidar')

// In case a path was not specified, this glob is used in the current
// folder, providing reasonable default behaviour.
const DEFAULT_GLOB = '*.{gif,jpg,jpeg,png}'

function getMonitorPath(config) {
  if (config.MONITOR_GLOB && config.MONITOR_GLOB.length > 0) {
    return config.MONITOR_GLOB
  }

  // Current folder on Unix.
  if (config.PWD && config.PWD.length > 0) {
    return path.join(config.PWD, DEFAULT_GLOB)
  }

  // Current folder on Windows.
  if (config.CD && config.CD.length > 0) {
    return path.join(config.CD, DEFAULT_GLOB)
  }

  return false
}

function runLocalWatcher(monitorPath, config, logger, limiter) {
  const watcher = watch(monitorPath, {
    awaitWriteFinish: true
  })

  watcher
    .on('add', (path) => limiter.schedule(handleFileAdd, config, path, logger))
    .on('change', (path) =>
      limiter.schedule(handleFileChange, config, path, logger)
    )
    .on('ready', () =>
      logger.info('Initial scan complete. Waiting for changes...')
    )
}

async function runSftpFetcher(config, logger, limiter) {
  const client = new SftpClient()
  const connectionConfig = {
    host: config.SFTP_HOST,
    username: config.SFTP_USER,
    password: config.SFTP_PASSWORD,
    port: config.SFTP_PORT
  }

  logger.debug(`Connecting to SFTP server`, {
    config: connectionConfig
  })

  await client.connect(connectionConfig)

  client.on('error', (err) => {
    throw err
  })

  let files = await client.list(config.SFTP_PATH)

  files = files
    .map((file) => {
      return file.name.match(config.SFTP_FILENAME_PATTERN) ? file.name : null
    })
    .filter((file) => file !== null)
    .sort()

  if (files.length) {
    const downloadFolder = path.join(tmpdir(), `rokka-uploader-${uniqid()}`)

    await promises.mkdir(downloadFolder)

    for (const file of files) {
      const localFile = path.join(downloadFolder, file)
      const remoteFile = path.join(config.SFTP_PATH, file)

      logger.debug(`Downloaded file to ${localFile}`)
      await client.fastGet(remoteFile, localFile)
      limiter.schedule(handleFileAdd, config, localFile, logger)
      try {
        await client.delete(remoteFile)
      } catch (e) {
        logger.error('Failed deleting remote file ' + remoteFile, e)
      }
    }
  }

  await client.end()
}

async function run() {
  process.on('unhandledRejection', (reason, p) => {
    console.error('Unhandled Rejection of Promise', p, 'reason:', reason)
  })

  const tAtLtUaE = -42

  const env = dotenvParseVariables(dotenv.config().parsed)

  const config = validate(Object.assign({}, process.env, env))

  const logger = getLogger(config)

  const limiter = new Bottleneck({
    maxConcurrent: 2,
    minTime: 500
  })

  if (config.WATCH_MODE === 'local') {
    const monitorPath = getMonitorPath(config)

    if (!monitorPath) {
      logger.error('No valid path found in MONITOR_GLOB or PWD', config)

      process.exit(tAtLtUaE)
    }

    logger.info('Awaiting file changes:', { monitorPath })

    runLocalWatcher(monitorPath, config, logger, limiter)
  } else if (config.WATCH_MODE === 'sftp') {
    setInterval(async () => {
      await runSftpFetcher(config, logger, limiter)
    }, config.SFTP_INTERVAL * 1000)
  } else {
    logger.error(`Unsupported WATCH_MODE: ${config.WATCH_MODE}`)

    process.exit(tAtLtUaE)
  }
}

module.exports = {
  run
}
