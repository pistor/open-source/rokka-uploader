const fs = require('fs')
const path = require('path')
const rokka = require('rokka')
const { getExistingImage } = require('./fileDeduplication')
const { pingSender } = require('./pingSender')

// eslint-disable-next-line no-control-regex
const disallowedCharacterMatcher = /\t\n\r\0\x0B/

async function handleFileAdd(config, filePath, logger) {
  logger.info('Handling new file:', { filePath })

  await processFile(config, filePath, logger)
}

async function handleFileChange(config, filePath, logger) {
  logger.info('Handling changed file', { filePath })

  await processFile(config, filePath, logger)
}

async function processFile(config, filePath, logger) {
  const fileName = path
    .basename(filePath)
    // Most compatible unicode normalisation.
    .normalize('NFKC')
    // Remove bad whitespace anywhere in file name.
    .replace(disallowedCharacterMatcher, '')
    // Also remove spaces at either end.
    .trim()
  const rokkaClient = rokka({ apiKey: config.ROKKA_API_KEY })

  if (config.DEDUPLICATE_BEFORE_UPLOAD) {
    const existingImage = await getExistingImage(config, filePath, rokkaClient)

    if (!existingImage) {
      uploadFile(config, fileName, filePath, logger, rokkaClient)
    } else {
      logger.info('Image already exists on rokka, skipping upload.', {
        fileName,
        existingImage
      })

      await updateUserMetadata(
        config,
        rokkaClient,
        stripFileNameExtension(fileName),
        existingImage.hash,
        existingImage.user_metadata
      )

      const pingUrls = Array.isArray(config.PING_URL)
        ? config.PING_URL
        : [config.PING_URL]

      if (pingUrls.length) {
        for (const pingUrl of pingUrls) {
          try {
            const pingResponse = await pingSender(
              pingUrl,
              config,
              fileName,
              existingImage,
              'duplicate'
            )
            if (!pingResponse.success) {
              logger.error('Ping sending failed', pingResponse)
            }
          } catch (e) {
            logger.error(
              `Ping sending failed after ${config.PING_RETRY_COUNT} retries.`
            )
          }
        }
      }

      if (config.DELETE_DUPLICATES) {
        logger.info('Deleting file', { filePath })
        fs.unlink(filePath, (err) => {
          if (err) {
            logger.error('Failed deleting file ' + filePath, err)
          }
        })
      }
    }
  } else {
    uploadFile(config, fileName, filePath, logger, rokkaClient)
  }
}

function stripFileNameExtension(fileName) {
  return path.parse(fileName).name
}

async function updateUserMetadata(
  config,
  rokkaClient,
  fileName,
  hash,
  existingUserMetadata
) {
  let changed = false
  const userMetadata = existingUserMetadata || {}

  if (config.METADATA_FILENAMES_KEY) {
    // If array already exists, update it.
    if (userMetadata[config.METADATA_FILENAMES_KEY]) {
      if (!userMetadata[config.METADATA_FILENAMES_KEY].includes(fileName)) {
        userMetadata[config.METADATA_FILENAMES_KEY].push(fileName)
        changed = true
      }
    } else {
      userMetadata[config.METADATA_FILENAMES_KEY] = [fileName]
      changed = true
    }
  }

  changed = addStaticMetadata(config, userMetadata, changed)

  if (changed) {
    await rokkaClient.sourceimages.meta.add(
      config.ROKKA_ORGANISATION,
      hash,
      userMetadata
    )
  }
}

async function uploadFile(config, fileName, filePath, logger, rokkaClient) {
  const metadata = {
    meta_user: {}
  }

  if (config.METADATA_FILENAMES_KEY) {
    metadata.meta_user[config.METADATA_FILENAMES_KEY] = [
      stripFileNameExtension(fileName)
    ]
  }

  addStaticMetadata(config, metadata.meta_user)

  try {
    const result = await rokkaClient.sourceimages.create(
      config.ROKKA_ORGANISATION,
      fileName,
      fs.createReadStream(filePath),
      metadata
    )

    // To see if the upload succeeded, we need to dig into this complex
    // request/response object a bit.
    if (result.body && result.body.items && result.body.items[0]) {
      const response = result.body.items[0]

      logger.info('Upload of file succeeded, stored with hash', {
        fileName,
        filePath,
        hash: response.hash
      })

      const pingUrls = Array.isArray(config.PING_URL)
        ? config.PING_URL
        : [config.PING_URL]

      if (pingUrls.length) {
        for (const pingUrl of pingUrls) {
          try {
            const pingResponse = await pingSender(
              pingUrl,
              config,
              fileName,
              response,
              'upload'
            )

            if (!pingResponse.success) {
              logger.error('Ping sending failed', pingResponse)
            }
          } catch (e) {
            logger.error(
              `Ping sending failed after ${config.PING_RETRY_COUNT} retries.`
            )
          }
        }
      }

      if (config.DELETE_AFTER_SUCCESSFUL_UPLOAD) {
        logger.info('Deleting file', { filePath })
        fs.unlink(filePath, (err) => {
          if (err) {
            logger.error('Failed deleting file ' + filePath, err)
          }
        })
      }
    } else {
      logger.error('File upload to Rokka failed', {
        filePath,
        error: result.error
      })
    }
  } catch (err) {
    logger.error('File upload to Rokka threw an error', {
      filePath,
      error: err.error
    })
  }
}

function addStaticMetadata(config, userMetadata, changedBefore = false) {
  let changed = changedBefore

  if (
    typeof config.METADATA_STATIC_TAG_KEY === 'string' &&
    typeof config.METADATA_STATIC_TAG_VALUE === 'string'
  ) {
    if (!userMetadata[config.METADATA_STATIC_TAG_KEY]) {
      userMetadata[config.METADATA_STATIC_TAG_KEY] =
        config.METADATA_STATIC_TAG_VALUE
      changed = true
    }
  } else if (
    Array.isArray(config.METADATA_STATIC_TAG_KEY) &&
    Array.isArray(config.METADATA_STATIC_TAG_VALUE) &&
    config.METADATA_STATIC_TAG_KEY.length ===
      config.METADATA_STATIC_TAG_VALUE.length
  ) {
    for (let i = 0; i < config.METADATA_STATIC_TAG_KEY.length; i++) {
      const key = config.METADATA_STATIC_TAG_KEY[i]

      if (!userMetadata[key]) {
        userMetadata[key] = config.METADATA_STATIC_TAG_VALUE[i]
        changed = true
      }
    }
  }

  return changed
}

module.exports = { handleFileAdd, handleFileChange }
