const winston = require('winston')

function getLogger(config) {
  const logger = winston.createLogger({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.simple()
    ),
    level: 'error',
    transports: [
      new winston.transports.File({
        filename: 'rokka-uploader-error.log',
        level: 'error'
      })
    ]
  })

  if (config.CONSOLE_LOG_LEVEL) {
    logger.add(
      new winston.transports.Console({
        format: winston.format.simple(),
        level: config.CONSOLE_LOG_LEVEL
      })
    )
  }

  return logger
}

module.exports = { getLogger }
