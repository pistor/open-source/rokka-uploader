const fetch = require('node-fetch')
const promiseRetry = require('promise-retry')
const { createHmac } = require('crypto')

/**
 * Create HMAC digest of payload.
 *
 * @param {string} payload Payload to digest.
 * @param {string} secret Secret used to validate digest.
 *
 * @return {string} Hex-encoded digest.
 */
function digest(payload, secret) {
  const hmac = createHmac('sha256', secret)
  hmac.update(payload)
  return hmac.digest('hex')
}

function patchMetaDataKeysInRokkaResponse(config, rokkaResponse) {
  let keys = []

  if (typeof config.METADATA_STATIC_TAG_KEY === 'string') {
    keys.push(config.METADATA_STATIC_TAG_KEY)
  } else if (Array.isArray(config.METADATA_STATIC_TAG_KEY)) {
    keys = config.METADATA_STATIC_TAG_KEY
  }

  if (keys.length > 0) {
    for (const key of keys) {
      const metaDataKeySplit = key.split(':')

      if (metaDataKeySplit.length >= 2) {
        const metaDataKey = metaDataKeySplit[1]

        if (
          rokkaResponse.user_metadata &&
          Object.keys(rokkaResponse.user_metadata).includes(metaDataKey)
        ) {
          rokkaResponse.user_metadata[key] =
            rokkaResponse.user_metadata[metaDataKey]

          delete rokkaResponse.user_metadata[metaDataKey]
        }
      }
    }
  }

  return rokkaResponse
}

/**
 * Sends a ping to the configured PING URL.
 *
 * "Give me a ping, Vasili. One ping only, please."
 *
 * Sends along the Rokka metadata for the upload.
 *
 * @param {string} postUrl
 * @param {string} rokkaApiKey
 * @param {string} fileName
 * @param {object} rokkaResponse
 * @param {string} pingType Ping type, should be `upload` or `duplicate`.
 * @returns {Promise<{success: {boolean}, status: {*}}, text: {string}>}
 */
async function pingSender(postUrl, config, fileName, rokkaResponse, pingType) {
  return promiseRetry(
    async (retry) => {
      const pingContent = Object.assign(
        { fileName, pingType },
        patchMetaDataKeysInRokkaResponse(config, rokkaResponse)
      )
      const payload = JSON.stringify(pingContent)

      const headers = {
        'Auth-Digest': digest(payload, config.ROKKA_API_KEY),
        'Content-Type': 'application/json'
      }

      const response = await fetch(postUrl, {
        method: 'POST',
        body: payload,
        headers
      })

      if (!(response.status >= 200 && response.status < 400)) {
        return retry()
      }

      return {
        success: response.ok,
        status: response.status
      }
    },
    {
      retries: config.PING_RETRY_COUNT,
      factor: 1,
      minTimeout: config.PING_RETRY_WAIT_TIME * 1000
    }
  )
}

module.exports = {
  pingSender,
  patchMetaDataKeysInRokkaResponse
}
